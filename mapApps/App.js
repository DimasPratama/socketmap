

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput} from 'react-native';
import MapView from 'react-native-maps';


export default class App extends Component{
    constructor(props){
        super(props)
        this.state={

            error:"",
            latitude:0,
            longitude:0,
            destination:'',
            predictions:[]
        }
    }

    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    latitude:position.coords.latitude,
                    longitude:position.coords.longitude
                })
            },
            error => this.setState({error:error.message}),
            {enableHighAccuracy:true,maximumAge:2000, timeout:20000}
        )
    }
    async onChangeDestination (destination){
        const apikey="AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw"
        const apiUrl=`https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${destination}&location=${this.state.latitude}, ${this.state.longitude}&radius=2000`

    try{
        const result = await fetch(apiUrl)
        const json = await result.json()
        this.setState({predictions:json.predictions})
    }catch(e){
    console.log(e)
    }
}


    render() {
        const predictions = this.state.predictions.map(prediction =>(
            <Text style={styles.suggestion} key={prediction.id}>{prediction.description}</Text>
        ))
    return (
        <View style={styles.container}>
          <MapView

              style={styles.map}
              region={{
                  latitude: this.state.latitude,
                  longitude: this.state.longitude,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
              }}
              showsUserLocation={true}
          >
          </MapView>
            <TextInput
            placeholder="Enter your Location ..."
            value={this.state.destination}
            style={styles.destination}
            onChangeText={destination=>{
                this.setState({destination})
                this.onChangeDestination(destination)

            }}

            />
            {predictions}
        </View>

    );
  }
}

const styles = StyleSheet.create({
    suggestion:{
        backgroundColor: "white",
        padding: 5,
        fontSize:18,
        borderWidth: 0.5,
        marginLeft: 5,
        marginRight: 5
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    destination:{
        height:40,
        borderWidth:0.5,
        marginRight:5,
        marginLeft:5,
        marginTop:50,
        padding:5,
        backgroundColor:"white"

    }
});