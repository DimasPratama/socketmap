

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,TextInput} from 'react-native';
import io from "socket.io-client"

export default class App extends Component {
  constructor(props){
    super(props)
      this.state={chatMessage:"",
          chatMessages:[]
    }
  }

  componentWillMount(){
    this.socket = io("http://10.10.103.138:3000")
    this.socket.on("chatMessage", msg =>{
      this.setState({chatMessages:[...this.state.chatMessages,msg]})
    })
  }
  submitChatMessage=()=>{
    this.socket.emit("chatMessage",this.state.chatMessage)
      this.setState({chatMessage:""})
  }

    render() {
    const chatMessages = this.state.chatMessages.map(chatMessage => (
          <Text key={chatMessage}>{chatMessage}</Text>
    ))
    return (
      <View style={styles.container}>
          <View style={{paddingTop:40}}>
            <TextInput
            style={{height:40, borderWidth:1}}
            autoCorrect={false}
            value={this.state.chatMessage}
            onChangeText={chatMessage=>{
              this.setState({chatMessage})
            }}
            onSubmitEditing={this.submitChatMessage}
            />
          </View>
          {chatMessages}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },

});
